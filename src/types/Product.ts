export interface Product {
  id: number;
  name: string;
  image: string;
  title: string;
  price: number;
  priceFormatted: string;
}
