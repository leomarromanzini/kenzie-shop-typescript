import {
  createContext,
  ReactNode,
  useContext,
  useState,
  useEffect,
} from "react";

import { Product } from "../../types/Product";

interface CartProviderProps {
  children: ReactNode;
}

interface CartProviderData {
  cart: Product[];
  setCart: (value: object) => void;
}

const CartContext = createContext<CartProviderData>({} as CartProviderData);

export const CartProvider = ({ children }: CartProviderProps) => {
  const cartItems = localStorage.getItem("cart");

  const parseCartItems = cartItems ? JSON.parse(cartItems) : [];

  const [cart, setCart] = useState(parseCartItems);

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  return (
    <CartContext.Provider value={{ cart, setCart }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
