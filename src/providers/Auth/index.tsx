import { createContext, ReactNode, useContext, useState } from "react";
import api from "../../services/api";
import { History } from "history";

interface AuthProviderProps {
  children: ReactNode;
}

interface SubmitData {
  username: string;
  password: string;
}

interface AuthProviderData {
  token: string;
  setAuth: (value: string) => void;
  signIn: (
    data: SubmitData,
    setError: (value: boolean) => void,
    history: History
  ) => void;
}

interface Token {
  access: string;
  refresh: string;
}

const AuthContext = createContext<AuthProviderData>({} as AuthProviderData);

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const token = localStorage.getItem("token") || "";

  const [auth, setAuth] = useState<string>(token);

  const signIn = (
    data: SubmitData,
    setError: (value: boolean) => void,
    history: History
  ) => {
    api
      .post<Token>("/sessions/", data)
      .then((response) => {
        localStorage.setItem("token", response.data.access);
        setAuth(response.data.access);
        history.push("/dashboard");
      })
      .catch((err) => setError(true));
  };

  return (
    <AuthContext.Provider value={{ token: auth, setAuth, signIn }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
